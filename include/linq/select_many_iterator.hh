#pragma once
#include <utility>
#include <functional>

#include "types.hh"

namespace linq
{
    template<typename iterator_type, typename container_type>
    class select_many_iterator_type
    {
    public:
        using value_type = iterator_value_type<iterator_type>;
        using selector_type = std::function<container_type(const value_type&)>;
        using container_iterator_type = typename container_type::iterator;
        using container_value_type = iterator_value_type<container_iterator_type>;

        select_many_iterator_type(iterator_type iterator, iterator_type end, selector_type selector):
            _iterator(std::move(iterator)),
            _end(std::move(end)),
            _selector(std::move(selector))
        {
            // Nothing to do yet
        }

        select_many_iterator_type(iterator_type iterator):
            _iterator(iterator),
            _end(std::move(iterator))
        {
            // Nothing to do yet
        }

        void begin()
        {
            _iterator.begin();
            _container = _selector(*_iterator);
            _container_iterator = _container.begin();
        }

        container_value_type operator*() const
        {
            return *_container_iterator;
        }

        select_many_iterator_type& operator++()
        {
            ++_container_iterator;
            if(_container_iterator == _container.end())
            {
                ++_iterator;
                if(_iterator != _end)
                {
                    _container = _selector(*_iterator);
                    _container_iterator = _container.begin();
                }
            }

            return *this;
        }

        bool operator!=(const select_many_iterator_type& other)
        {
            return _iterator != other._iterator;
        }

    private:
        iterator_type _iterator;
        iterator_type _end;
        selector_type _selector;
        container_type _container;
        container_iterator_type _container_iterator;
    };
}
