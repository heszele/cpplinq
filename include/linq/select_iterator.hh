#pragma once
#include <utility>
#include <functional>

#include "types.hh"

namespace linq
{
    template<typename iterator_type, typename result_type>
    class select_iterator_type
    {
    public:
        using value_type = iterator_value_type<iterator_type>;
        using selector_type = std::function<result_type(const value_type&)>;

        select_iterator_type(iterator_type iterator, selector_type selector):
            _iterator(std::move(iterator)),
            _selector(std::move(selector))
        {
            // Nothing to do yet
        }

        select_iterator_type(iterator_type iterator):
            _iterator(std::move(iterator))
        {
            // Nothing to do yet
        }

        void begin()
        {
            _iterator.begin();
        }

        result_type operator*() const
        {
            return _selector(*_iterator);
        }

        select_iterator_type& operator++()
        {
            ++_iterator;

            return *this;
        }

        bool operator!=(const select_iterator_type& other)
        {
            return _iterator != other._iterator;
        }

    private:
        iterator_type _iterator;
        selector_type _selector;
    };
}
