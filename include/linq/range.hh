#pragma once
#include "where_iterator.hh"
#include "select_iterator.hh"
#include "select_many_iterator.hh"
#include "of_type_iterator.hh"

/**
 * C# LINQ operators   C++ Linq operators
 * --------------------------------------
 * Filtering Operators
 * --------------------------------------
 * Where               where
 * OfType              of_type
 * --------------------------------------
 * Join Operators
 * --------------------------------------
 * Join                *None*
 * GroupJoin           *None*
 * --------------------------------------
 * Projection Operators
 * --------------------------------------
 * Select              select
 * SelectMany          *None*
 * --------------------------------------
 * Sorting Operators
 * --------------------------------------
 * OrderBy             *None*
 * OrderByDescending   *None*
 * ThenBy              *None*
 * ThenByDescending    *None*
 * Reverse             *None*
 * --------------------------------------
 * Grouping Operators
 * --------------------------------------
 * GroupBy             *None*
 * ToLookUp            *None*
 * --------------------------------------
 * Conversions Operators
 * --------------------------------------
 * AsEnumerable        *None*
 * AsQueryable         *None*
 * Cast                *None*
 * ToArray             *None*
 * ToDictionary        *None*
 * ToList              *None*
 * --------------------------------------
 * Concatenation Operators
 * --------------------------------------
 * Concat              *None*
 * --------------------------------------
 * Aggregation Operators
 * --------------------------------------
 * Aggregate           *None*
 * Avarage             *None*
 * Count               *None*
 * LongCount           *None*
 * Max                 *None*
 * Min                 *None*
 * Sum                 *None*
 * --------------------------------------
 * Quantifier Operators
 * --------------------------------------
 * All                 *None*
 * Any                 *None*
 * Contains            *None*
 * --------------------------------------
 * Partition Operators
 * --------------------------------------
 * Skip                *None*
 * SkipWhile           *None*
 * Take                *None*
 * TakeWhile           *None*
 * --------------------------------------
 * Generation Operators
 * --------------------------------------
 * DefaultIfEmpty      *None*
 * Empty               *None*
 * Range               *None*
 * Repeat              *None*
 * --------------------------------------
 * Set Operators
 * --------------------------------------
 * Distinct            *None*
 * Except              *None*
 * Intersect           *None*
 * Union               *None*
 * --------------------------------------
 * Equality Operators
 * --------------------------------------
 * SequenceEqual       *None*
 * --------------------------------------
 * Element Operators
 * --------------------------------------
 * ElementAt           *None*
 * ElementAtOrDefault  *None*
 * First               *None*
 * FirstOrDefault      *None*
 * Last                *None*
 * LastOrDefault       *None*
 * Single              *None*
 * SingleOrDefault     *None*
 */

namespace linq
{
    template<typename iterator_type>
    class range_type
    {
    public:
        range_type(iterator_type begin, iterator_type end):
            _begin(std::move(begin)),
            _end(std::move(end))
        {
            // Nothing to do yet
        }

        iterator_type begin() const
        {
            iterator_type begin(_begin);

            begin.begin();

            return begin;
        }

        iterator_type end() const
        {
            return _end;
        }

        range_type<where_iterator_type<iterator_type>> where(typename where_iterator_type<iterator_type>::predicate_type predicate) const
        {
            return { where_iterator_type<iterator_type>(_begin, _end, std::move(predicate)), where_iterator_type<iterator_type>(_end) };
        }

        template<typename result_type>
        range_type<select_iterator_type<iterator_type, result_type>> select(typename select_iterator_type<iterator_type, result_type>::selector_type selector) const
        {
            return { select_iterator_type<iterator_type, result_type>(_begin, std::move(selector)), select_iterator_type<iterator_type, result_type>(_end) };
        }

        template<typename container_type>
        range_type<select_many_iterator_type<iterator_type, container_type>> select_many(typename select_many_iterator_type<iterator_type, container_type>::selector_type selector) const
        {
            return { select_many_iterator_type<iterator_type, container_type>(_begin, _end, std::move(selector)), select_many_iterator_type<iterator_type, container_type>(_end) };
        }

        template<typename type>
        range_type<of_type_iterator_type<iterator_type, type>> of_type()
        {
            return { of_type_iterator_type<iterator_type, type>(_begin, _end), of_type_iterator_type<iterator_type, type>(_end) };
        }

    private:
        iterator_type _begin;
        iterator_type _end;
    };
}
