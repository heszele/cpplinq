#pragma once
#include <utility>
#include <functional>

#include "types.hh"

namespace linq
{
    template<typename iterator_type>
    class where_iterator_type
    {
    public:
        using value_type = iterator_value_type<iterator_type>;
        using predicate_type = std::function<bool(const value_type&)>;

        where_iterator_type(iterator_type iterator, iterator_type end, predicate_type predicate):
            _iterator(std::move(iterator)),
            _end(std::move(end)),
            _predicate(std::move(predicate))
        {
            // Nothing to do yet
        }

        where_iterator_type(iterator_type end):
            _iterator(end),
            _end(std::move(end))
        {
            // Nothing to do yet
        }

        void begin()
        {
            _iterator.begin();
            advance();
        }

        value_type operator*() const
        {
            return *_iterator;
        }

        where_iterator_type& operator++()
        {
            ++_iterator;
            advance();

            return *this;
        }

        bool operator!=(const where_iterator_type& other)
        {
            return _iterator != other._iterator;
        }

    private:
        void advance()
        {
            while(_iterator != _end)
            {
                if(_predicate(*_iterator))
                {
                    break;
                }
                ++_iterator;
            }
        }

        iterator_type _iterator;
        iterator_type _end;
        predicate_type _predicate;
    };
}
