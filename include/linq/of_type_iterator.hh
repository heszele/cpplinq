#include <type_traits>

#include "where_iterator.hh"
#include "types.hh"

namespace linq
{
    template<typename iterator_type, typename type>
    class of_type_iterator_type
    {
    public:
        using value_type = iterator_value_type<iterator_type>;

        static_assert(std::is_pointer<typename std::decay<value_type>::type>::value, "Only pointers can be used with of_type");
        static_assert(std::is_pointer<typename std::decay<type>::type>::value, "Only pointers can be used with of_type");

        of_type_iterator_type(iterator_type iterator, iterator_type end):
            _iterator(std::move(iterator), std::move(end), [](const value_type& value) { return dynamic_cast<type>(value) != nullptr; })
        {
            // Nothing to do yet
        }

        of_type_iterator_type(iterator_type iterator):
            _iterator(std::move(iterator))
        {
            // Nothing to do yet
        }

        void begin()
        {
            _iterator.begin();
        }

        value_type operator*() const
        {
            return *_iterator;
        }

        of_type_iterator_type& operator++()
        {
            ++_iterator;

            return *this;
        }

        bool operator!=(const of_type_iterator_type& other)
        {
            return _iterator != other._iterator;
        }

    private:
        where_iterator_type<iterator_type> _iterator;
    };
}