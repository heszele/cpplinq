#pragma once
#include <utility>

#include "types.hh"

namespace linq
{
    template<typename iterator_type>
    class wrapper_iterator_type
    {
    public:
        using value_type = iterator_value_type<iterator_type>;

        wrapper_iterator_type(iterator_type iterator):
            _iterator(std::move(iterator))
        {
            // Nothing to do yet
        }

        void begin()
        {
            // Nothing to do
        }

        value_type operator*() const
        {
            return *_iterator;
        }

        wrapper_iterator_type& operator++()
        {
            ++_iterator;
            return *this;
        }

        bool operator!=(const wrapper_iterator_type& other)
        {
            return _iterator != other._iterator;
        }

    private:
        iterator_type _iterator;
    };
}
