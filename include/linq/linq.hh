#pragma once
#include "wrapper_iterator.hh"
#include "range.hh"

namespace linq
{
    template<typename container_type>
    range_type<wrapper_iterator_type<typename container_type::const_iterator>> from(const container_type& container)
    {
        return { container.begin(), container.end() };
    }
}
