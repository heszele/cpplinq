#pragma once
#include <utility>

namespace linq
{
    template<typename iterator_type>
    using iterator_value_type = decltype(*std::declval<iterator_type>());
}
