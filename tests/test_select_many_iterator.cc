#include <catch2/catch.hpp>
#include <linq/linq.hh>

#include "utilities.hh"

SCENARIO("select_many() can be used easily")
{
    GIVEN("An std::vector<person_type>")
    {
        std::vector<person_type> people = {
            { "A", 11, { "A1", "A2" } },
            { "B", 22, { "B1", "B2" } },
            { "C", 33, { "C1", "C2" } },
            { "D", 44, { "D1", "D2" } },
            { "E", 55, { "E1", "E2" } },
        };

        AND_GIVEN("A range created from this std::vector<person_type>")
        {
            auto range = linq::from(people);

            THEN("This range can be used to select and flatten addresses on each element")
            {
                auto addresses = range.select_many<std::vector<std::string>>([](const person_type& person) { return person.get_addresses(); });
                std::vector<std::string> expected_addresses = {
                    "A1",
                    "A2",
                    "B1",
                    "B2",
                    "C1",
                    "C2",
                    "D1",
                    "D2",
                    "E1",
                    "E2",
                };

                check_range(addresses, expected_addresses);
            }
        }
    }
}