#include <catch2/catch.hpp>
#include <vector>
#include <list>
#include <map>
#include <linq/linq.hh>

#include "utilities.hh"

SCENARIO("linq::range objects can easily be created from standard containers")
{
    GIVEN("An std::vector<int>")
    {
        std::vector<int> values = { 1, 2, 3, 4, 5 };

        THEN("A range can be easily created from it")
        {
            auto range = linq::from(values);

            AND_THEN("The range can easily be used in for loops")
            {
                check_range(range, values);
            }
        }
    }
    GIVEN("An std::list<int>")
    {
        std::list<int> values = { 1, 2, 3, 4, 5 };

        THEN("A range can be easily created from it")
        {
            auto range = linq::from(values);

            AND_THEN("The range can easily be used in for loops")
            {
                check_range(range, values);
            }
        }
    }
    GIVEN("An std::map<int, float>")
    {
        std::map<int, float> values = {
            { 1, 1.1f },
            { 2, 2.2f },
            { 3, 3.3f },
            { 4, 4.4f },
            { 5, 5.5f },
        };

        THEN("A range can be easily created from it")
        {
            auto range = linq::from(values);

            AND_THEN("The range can easily be used in for loops")
            {
                check_range(range, values);
            }
        }
    }
}
