#include <catch2/catch.hpp>
#include <vector>
#include <linq/linq.hh>

#include "utilities.hh"

SCENARIO("select() can easily be used")
{
    GIVEN("An std::vector<person_type>")
    {
        std::vector<person_type> people = {
            { "A", 11 },
            { "B", 22 },
            { "C", 33 },
            { "D", 44 },
            { "E", 55 },
        };

        AND_GIVEN("A range created from this std::vector<person_type>")
        {
            auto range = linq::from(people);

            THEN("This range can be used to select age on every element")
            {
                auto ages = range.select<uint32_t>([](const person_type& person) { return person.get_age(); });
                std::vector<uint32_t> expected_ages = { 11, 22, 33, 44, 55 };

                check_range(ages, expected_ages);
            }
            THEN("This range can be used to select name on every element")
            {
                auto names = range.select<std::string>([](const person_type& person) { return person.get_name(); });
                std::vector<std::string> expected_names = { "A", "B", "C", "D", "E" };

                check_range(names, expected_names);
            }
        }
    }
}
