#include <catch2/catch.hpp>
#include <vector>
#include <linq/linq.hh>

#include "utilities.hh"

SCENARIO("where() can easily be used")
{
    GIVEN("An std::vector<person_type>")
    {
        std::vector<person_type> people = {
            { "A", 11 },
            { "B", 22 },
            { "C", 33 },
            { "D", 44 },
            { "E", 55 },
        };

        AND_GIVEN("A range created from this std::vector<person_type>")
        {
            auto range = linq::from(people);

            THEN("This range can be used to filter elements on age")
            {
                auto adults = range.where([](const person_type& person) { return person.get_age() > 18; });
                std::vector<person_type> expected_adults = {
                    { "B", 22 },
                    { "C", 33 },
                    { "D", 44 },
                    { "E", 55 },
                };

                check_range(adults, expected_adults);
            }
            THEN("This range can be used to filter elements on name")
            {
                auto firsts = range.where([](const person_type& person) { return person.get_name() < "D"; });
                std::vector<person_type> expected_firsts = {
                    { "A", 11 },
                    { "B", 22 },
                    { "C", 33 },
                };

                check_range(firsts, expected_firsts);
            }
            THEN("If no elements are fulfilling the predicate, where() will return no elements")
            {
                auto empty = range.where([](const person_type& person) { return person.get_name() > "E"; });
                uint32_t counter = 0;

                for(auto person: empty)
                {
                    ++counter;
                }

                CHECK(counter == 0);
            }
        }
    }
}
