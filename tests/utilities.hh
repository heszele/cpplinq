#pragma once
#include <string>
#include <vector>
#include <type_traits>

template<typename range_type, typename container_type>
typename std::enable_if<!std::is_pointer<typename container_type::value_type>::value, void>::type check_range(const range_type& range, const container_type& values)
{
    {
        auto container_iterator = values.begin();

        for(auto value: range)
        {
            REQUIRE(container_iterator != values.end());
            CHECK(value == *container_iterator);
            ++container_iterator;
        }
    }
    {
        auto container_iterator = values.begin();

        for(auto iterator = range.begin(); iterator != range.end(); ++iterator)
        {
            REQUIRE(container_iterator != values.end());
            CHECK(*iterator == *container_iterator);
            ++container_iterator;
        }
    }
}

template<typename range_type, typename container_type>
typename std::enable_if<std::is_pointer<typename container_type::value_type>::value, void>::type check_range(const range_type& range, const container_type& values)
{
    {
        auto container_iterator = values.begin();

        for(auto value: range)
        {
            REQUIRE(container_iterator != values.end());
            CHECK(*value == **container_iterator);
            ++container_iterator;
        }
    }
    {
        auto container_iterator = values.begin();

        for(auto iterator = range.begin(); iterator != range.end(); ++iterator)
        {
            REQUIRE(container_iterator != values.end());
            CHECK(**iterator == **container_iterator);
            ++container_iterator;
        }
    }
}

class person_type
{
public:
    person_type(std::string name, uint32_t age, std::vector<std::string> addresses = {}):
        _name(std::move(name)),
        _age(age),
        _addresses(std::move(addresses))
    {
        // Nothing to do yet
    }

    virtual ~person_type() = default;

    const std::string& get_name() const
    {
        return _name;
    }

    void set_name(std::string name)
    {
        _name = std::move(name);
    }

    uint32_t get_age() const
    {
        return _age;
    }

    void set_age(uint32_t age)
    {
        _age = age;
    }
    const std::vector<std::string>& get_addresses() const
    {
        return _addresses;
    }

    void add_address(std::string address)
    {
        _addresses.emplace_back(std::move(address));
    }

    bool operator==(const person_type& person) const
    {
        return get_name() == person.get_name() && get_age() == person.get_age();
    }

private:
    std::string _name;
    uint32_t _age = 0;
    std::vector<std::string> _addresses;
};
