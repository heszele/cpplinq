#include <catch2/catch.hpp>
#include <vector>
#include <linq/linq.hh>

#include "utilities.hh"

namespace
{
    class child_type: public person_type
    {
    public:
        using person_type::person_type;

    private:
        person_type* _mother = nullptr;
        person_type* _father = nullptr;
    };
}

SCENARIO("of_type() can easily be used")
{
    GIVEN("An std::vector<person_type*>")
    {
        std::vector<person_type*> people = {
            new person_type{ "A", 11 },
            new child_type{ "B", 22 },
            new person_type{ "C", 33 },
            new child_type{ "D", 44 },
            new person_type{ "E", 55 },
        };

        AND_GIVEN("A range created from this std::vector<person_type>")
        {
            auto range = linq::from(people);

            THEN("This range can be used to select only a certain types")
            {
                auto children = range.of_type<child_type*>();
                std::vector<child_type*> expected_children = {
                    new child_type{ "B", 22 },
                    new child_type{ "D", 44 },
                };

                check_range(children, expected_children);
            }
            THEN("of_type() returns parent types as well")
            {
                auto children = range.of_type<person_type*>();

                check_range(children, people);
            }
            THEN("If no elements found with the given type, of_type() will return no elements")
            {
                auto empty = range.of_type<std::string*>();
                uint32_t counter = 0;

                for(auto person: empty)
                {
                    ++counter;
                }

                CHECK(counter == 0);
            }
        }
    }
}