#include <catch2/catch.hpp>
#include <linq/linq.hh>

#include "utilities.hh"

SCENARIO("linq queries are evaluated lazily")
{
    GIVEN("An std::vector<person_type>")
    {
        std::vector<person_type> people = {
            { "A", 11 },
            { "B", 22 },
            { "C", 33 },
            { "D", 44 },
            { "E", 55 },
        };

        AND_GIVEN("A range created from this std::vector<person_type>")
        {
            auto range = linq::from(people);

            THEN("The predicates will not be invoked until the results are queried")
            {
                uint32_t counter = 0;
                auto predicate = [&counter](const person_type& person) {
                    ++counter;
                    return person.get_age() >= 18;
                };
                auto adults = range.where(predicate);
                std::vector<person_type> expected_adults = {
                    { "B", 22 },
                    { "C", 33 },
                    { "D", 44 },
                    { "E", 55 },
                };

                CHECK(counter == 0);
                check_range(adults, expected_adults);
                // counter will be 10, since "check_range" iterates over the given range twice
                CHECK(counter == 10);
            }
            THEN("The selectors will not be invoked until the results are queried")
            {
                uint32_t counter = 0;
                auto selector = [&counter](const person_type& person) {
                    ++counter;
                    return person.get_age();
                };
                auto ages = range.select<uint32_t>(selector);
                std::vector<uint32_t> expected_ages = { 11, 22, 33, 44, 55 };

                CHECK(counter == 0);
                check_range(ages, expected_ages);
                // counter will be 10, since "check_range" iterates over the given range twice
                CHECK(counter == 10);
            }
        }
    }
}
